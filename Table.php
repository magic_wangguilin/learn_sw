<?php
/**
 * Created by PhpStorm.
 * User: 11893
 * Date: 2019/1/15
 * Time: 9:37
 */

class Table
{
    protected $table;

    public function __construct()
    {
        $table = new swoole_table(1024);
        $table->column('fd', swoole_table::TYPE_INT);
        $table->column('from_id', swoole_table::TYPE_INT);
        $table->column('id', swoole_table::TYPE_INT);
        $table->create();
        $this->table = $table;
    }
}