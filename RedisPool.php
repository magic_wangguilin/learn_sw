<?php
/**
 * Created by PhpStorm.
 * User: 11893
 * Date: 2019/1/15
 * Time: 18:17
 */

class RedisPool
{
    protected $pool;

    public function __construct()
    {
        $this->pool = new SplQueue();
    }

    public function get()
    {
        if ($this->pool->count() > 0) {
            return $this->pool->pop();
        }
        $redis = new Redis();
        if ($redis->connect('127.0.0.1')) {
            $this->pool->push($redis);
            return $redis;
        }
        return false;
    }

    public function put($redis)
    {
        $this->pool->push($redis);
    }
}