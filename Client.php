<?php
/**
 * Created by PhpStorm.
 * User: 11893
 * Date: 2019/1/14
 * Time: 17:06
 */

class Client
{
    protected $login = false;

    public function __construct()
    {
        $client = new swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);
        $client->on("connect", function (swoole_client $cli) {
//            $cli->send("GET / HTTP/1.1\r\n\r\n");
            $cli->send(json_encode(array('login' => true, 'id' => 1)));
        });
        $client->on("receive", function (swoole_client $cli, $data) {
            $this->login = true;
            echo "Receive: $data\n";
        });
        $client->on("error", function (swoole_client $cli) {
            echo "error\n";
        });
        $client->on("close", function (swoole_client $cli) {
            echo "Connection close\n";
        });
        if (!$client->connect('127.0.0.1', 9501)) {
            exit("connect failed. Error: {$client->errCode}\n");
        }
        swoole_event_add(STDIN, function ($fp) use ($client) {
//            $client->send(fread($fp, 8192));
            $msg = fread($fp, 8192);
            $client->send(json_encode(array('msg' => $msg, 'id' => 1)));
        });
    }
}

new Client();